# QML PROJECT

## Task:
Develop an interactive prototype of the application under development

Provide a collated layout of the application according to the developed prototype.

#

## Demonstration:

![0](images/0.gif "0")
![1](images/1.png "1")
![2](images/2.png "2")
![3](images/3.png "3")
![4](images/4.png "4")
![5](images/5.png "5")
![6](images/6.png "6")
![7](images/7.png "7")
![8](images/8.png "8")
![9](images/9.png "9")
![10](images/10.png "10")
![11](images/11.png "11")
![12](images/12.png "12")
![13](images/13.png "13")
![14](images/14.png "14")
![15](images/15.png "15")
![16](images/16.png "16")
![17](images/17.png "17")
![18](images/18.png "18")
![19](images/19.png "19")
![20](images/20.png "20")
![21](images/21.png "21")
![22](images/22.png "22")