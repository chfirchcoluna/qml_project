import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Rectangle {
    property string comText
    property bool comBold
    width: (parent.width - 30) / 8
    height: 30
    Text {
        font.pixelSize: 20
        color: "#606FF1"
        anchors.centerIn: parent
        text: comText
        font.bold: comBold
    }
}
