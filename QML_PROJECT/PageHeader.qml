import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


Item {
    width: 430
    height: 65
    Rectangle {
        anchors.fill: parent
        color: "white"
        RowLayout {
            spacing: 0
            anchors.fill: parent
            Image {
                source: "qrc:/images/header/burgerIcon.svg"
                width: 40
                height: 25
                Layout.leftMargin: 15
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        drawer.open();
                        console.log("Burger menu clicked!")
                    }
                }
            }
            RowLayout {
                Layout.alignment: Qt.AlignRight
                Image {
                    source: "qrc:/images/header/bellIcon.svg"
                    width: 28
                    height: 33
                    Layout.alignment: Qt.AlignLeft
                    Layout.rightMargin: 15
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Notification icon clicked!")
                            notificationPopup.open()
                        }
                    }
                }
                Image {
                    source: "qrc:/images/header/profileIcon.png"
                    width: 45
                    height: 45
                    Layout.alignment: Qt.AlignRight
                    //Layout.leftMargin: 0
                    Layout.rightMargin: 15
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Profile icon clicked!")
                        }
                    }
                }
            }
        }
    }

    Popup {
        id: notificationPopup
        width: 430
        height: 932
        opacity: 0
        modal: true
        Overlay.modal: Rectangle {
            color: "#1E1E1E00"
        }
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onOpened: {
            notificationPopup.opacity = 1;
        }

        onClosed: {
            notificationPopup.opacity = 0;
        }
        contentItem: Rectangle {
            width: parent.width
            height: parent.height
            anchors.fill: parent
            ColumnLayout {
                width: parent.width
                height: parent.height

                Rectangle {
                    width: parent.width
                    height: 40

                    RowLayout {
                        anchors.fill: parent
                        spacing: 10

                        Image {
                            Layout.topMargin: 10
                            source: "qrc:/images/header/backArrowNegativeIcon.svg"
                            width: 20
                            height: 20
                            Layout.leftMargin: 15
                            MouseArea {
                                anchors.fill: parent
                                onClicked: notificationPopup.close()
                            }
                        }

                        Item {
                            Layout.fillWidth: true
                        }

                        Image {
                            Layout.topMargin: 10
                            source: "qrc:/images/header/notificationsSettings.svg"
                            width: 28
                            height: 28
                            Layout.rightMargin: 15
                            MouseArea {
                                anchors.fill: parent
                                onClicked: console.log("Notifications settings clicked!")
                            }
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 40
                    RowLayout {
                        anchors.fill: parent
                        Rectangle {
                            width: 264
                            Layout.alignment: Qt.AlignHCenter
                            Text {
                                anchors.centerIn: parent
                                text: "Уведомления"
                                font.pointSize: 24
                                font.family: "Rubik"
                                font.weight: Font.Bold
                            }
                        }

                    }
                }


                Rectangle {
                    Layout.topMargin: 20
                    width: parent.width
                    height: 40
                    RowLayout {
                        anchors.fill: parent
                        Rectangle {
                            width: parent.width - 100
                            height: 50
                            Layout.alignment: Qt.AlignHCenter
                            border.width: 2
                            radius: 10
                            border.color: "#606FF1"
                            Text {
                                anchors.centerIn: parent
                                text: "Отметить всё как прочитанное"
                                color: "#606FF1"
                                font.pixelSize: 16
                                font.family: "Rubik"
                                font.weight: Font.Bold

                            }
                        }

                    }
                }

                Rectangle {
                    width: parent.width
                    Layout.leftMargin: 50
                    Layout.topMargin: 30
                    Layout.rightMargin: 50
                    height: 40
                    RowLayout {
                        Layout.fillWidth: true
                        spacing: 20

                        Rectangle {
                            width: 40
                            height: 40
                            Rectangle {
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: 15
                                width: 30
                                height: 5
                                color: "#606FF1"
                                radius: 10
                            }

                            Text {
                                text: "Все"
                                font.pixelSize: 16
                                font.family: "Rubik"
                                font.weight: Font.Bold
                            }
                        }

                        Rectangle {
                            width: 80
                            height: 40
                            Text {
                                text: "Мотивация"
                                font.pixelSize: 16
                                font.family: "Rubik"
                                font.weight: Font.Bold
                            }
                        }

                        Rectangle {
                            width: 80
                            height: 40
                            Text {
                                text: "Обучение"
                                font.pixelSize: 16
                                font.family: "Rubik"
                                font.weight: Font.Bold
                            }
                        }

                        Rectangle {
                            width: 80
                            height: 40
                            Text {
                                text: "Прочее"
                                font.pixelSize: 16
                                font.family: "Rubik"
                                font.weight: Font.Bold
                            }
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 1
                    color: "#B0C4DE"
                    layer.enabled: true
                        layer.effect: DropShadow {
                            horizontalOffset: 3
                            verticalOffset: 3
                            radius: 8
                            samples: 17
                            color: "#80000000"
                        }
                }

                Rectangle {
                    width: parent.width - 40
                    height: 1
                    color: "#B0C4DE"
                    Layout.topMargin: 20
                    Layout.leftMargin: 20
                    Layout.rightMargin: 20
                }

                Rectangle {
                    Layout.bottomMargin: 10
                    width: parent.width
                    height: 40
                    RowLayout {
                        anchors.fill: parent
                        Rectangle {
                            width: 264
                            Layout.alignment: Qt.AlignLeft
                            Text {
                                anchors.left: parent.left
                                anchors.leftMargin: 20
                                text: "26 октября"
                                font.pixelSize: 16
                                font.family: "Rubik"
                                color: "#606FF1"
                            }
                        }

                    }
                }

                Rectangle {
                    width: parent.width - 40
                    height: 1
                    color: "#B0C4DE"
                    Layout.leftMargin: 20
                    Layout.rightMargin: 20
                }

                Rectangle {
                    width: parent.width
                    height: 40
                    RowLayout {
                        anchors.fill: parent
                        Rectangle {
                            width: 264
                            Layout.alignment: Qt.AlignLeft
                            Text {
                                anchors.left: parent.left
                                anchors.leftMargin: 20
                                text: "26.10.23 10:00"
                                font.pixelSize: 10
                                font.family: "Rubik"
                                color: "#646464"
                            }
                        }

                    }
                }

                RowLayout {
                    spacing: 10
                    Layout.fillWidth: true
                    Layout.topMargin: 0
                    Rectangle {
                        width: 380
                        color: "transparent"
                        Text {
                            text: "Молодец, продолжай в том же духе! За прошлую неделю ты побил свой старый рекорд и сделал 20 заданий!"
                            font.pixelSize: 16
                            wrapMode: Text.WordWrap
                            width: parent.width - 20
                            font.family: "Rubik"
                            anchors.fill: parent
                            anchors.leftMargin: 20
                        }
                    }

                    Image {
                        source: "qrc:/images/header/checkmark.svg"
                        width: 20
                        height: 20
                    }
                }

                Rectangle {
                    width: parent.width - 40
                    height: 1
                    color: "#B0C4DE"
                    Layout.topMargin: 70
                    Layout.leftMargin: 20
                    Layout.rightMargin: 20
                }

                Rectangle {
                    width: parent.width
                    Layout.bottomMargin: 10
                    height: 40
                    RowLayout {
                        anchors.fill: parent
                        Rectangle {
                            width: 264
                            Layout.alignment: Qt.AlignLeft
                            Text {
                                anchors.left: parent.left
                                anchors.leftMargin: 20
                                text: "23 октября"
                                font.pixelSize: 16
                                font.family: "Rubik"
                                color: "#606FF1"
                            }
                        }

                    }
                }

                Rectangle {
                    width: parent.width - 40
                    height: 1
                    color: "#B0C4DE"
                    Layout.leftMargin: 20
                    Layout.rightMargin: 20
                }

                Rectangle {
                    width: parent.width
                    height: 40
                    RowLayout {
                        anchors.fill: parent
                        Rectangle {
                            width: 264
                            Layout.alignment: Qt.AlignLeft
                            Text {
                                anchors.left: parent.left
                                anchors.leftMargin: 20
                                text: "23.10.23 15:20"
                                font.pixelSize: 10
                                font.family: "Rubik"
                                color: "#646464"
                            }
                        }

                    }
                }

                RowLayout {
                    spacing: 10
                    Layout.fillWidth: true
                    Layout.topMargin: 10
                    Rectangle {
                        width: 380
                        color: "transparent"
                        Text {
                            text: "Скоро (24.10.23) истекает дедлайн по заданию"
                            font.pixelSize: 16
                            wrapMode: Text.WordWrap
                            width: parent.width - 20
                            font.family: "Rubik"
                            anchors.fill: parent
                            anchors.leftMargin: 20
                        }
                        Text {
                            text: "Позвонить маме"
                            font.pixelSize: 16
                            color: "#606FF1"
                            wrapMode: Text.WordWrap
                            width: parent.width - 20
                            font.family: "Rubik"
                            anchors.fill: parent
                            anchors.topMargin: 20
                            anchors.leftMargin: 20
                        }

                    }

                    Image {
                        source: "qrc:/images/header/checkmark.svg"
                        width: 20
                        height: 20
                    }
                }

                Rectangle {
                    Layout.topMargin: 50
                    width: parent.width - 40
                    height: 1
                    color: "#B0C4DE"
                    Layout.leftMargin: 20
                    Layout.rightMargin: 20
                }

                Item {
                    Layout.fillHeight: true
                }


            }
        }}

    Drawer {
        id: drawer
        width: 350
        height: parent.height

        ColumnLayout {
            spacing: 10
            anchors.fill: parent

            Image {
                source: "qrc:/images/header/closeBurgerIcon.svg"
                width: 30
                height: 30
                Layout.leftMargin: 15
                Layout.topMargin: 22

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        drawer.close();
                    }
                }
            }

            RowLayout {
                width: parent.width
                height: 40
                Layout.topMargin: 25
                Layout.fillWidth: true

                Rectangle {
                    width: 130
                    height: 40
                    color: "transparent"

                    Text {
                        leftPadding: 22
                        color: "#606FF1"
                        font.pixelSize: 24
                        anchors.left: parent.left
                        text: "Премиум"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Navigate to: Премиум");
                            premiumPopup.open();
                        }
                    }
                }

                Image {
                    Layout.alignment: Qt.AlignRight
                    source: "qrc:/images/header/hotIcon.svg"
                    width: 22
                    height: 28
                    Layout.bottomMargin: 20
                }
            }

            RowLayout {
                width: parent.width
                height: 40
                Layout.fillWidth: true

                Rectangle {
                    width: 185
                    height: 40
                    color: "transparent"

                    Text {
                        leftPadding: 22
                        font.pixelSize: 24
                        font.family: "Rubik"
                        anchors.left: parent.left
                        text: "Сменить тему"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Navigate to: Сменить тему");
                            drawer.close();
                        }
                    }
                }

                Image {
                    Layout.alignment: Qt.AlignRight
                    source: "qrc:/images/header/crownIcon.svg"
                    width: 26
                    height: 23
                    Layout.bottomMargin: 15
                }
            }

            Item {
                width: parent.width
                height: 40

                Rectangle {
                    width: parent.width
                    height: 40
                    color: "transparent"

                    Text {
                        leftPadding: 22
                        font.pixelSize: 24
                        font.family: "Rubik"
                        anchors.left: parent.left
                        text: "Настройки уведомлений"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Navigate to: Настройки уведомлений");
                            drawer.close();
                        }
                    }
                }
            }

            Item {
                width: parent.width
                height: 40

                Rectangle {
                    width: parent.width
                    height: 40
                    color: "transparent"

                    Text {
                        leftPadding: 22
                        font.pixelSize: 24
                        font.family: "Rubik"
                        anchors.left: parent.left
                        text: "Написать в поддержку"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Navigate to: Написать в поддержку");
                            drawer.close();
                        }
                    }
                }
            }

            Item {
                width: parent.width
                height: 40

                Rectangle {
                    width: parent.width
                    height: 40
                    color: "transparent"

                    Text {
                        leftPadding: 22
                        font.pixelSize: 24
                        font.family: "Rubik"
                        anchors.left: parent.left
                        text: "Оценить приложение"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Navigate to: Оценить приложение");
                            drawer.close();
                        }
                    }
                }
            }

            Item {
                width: parent.width
                height: 40

                Rectangle {
                    width: parent.width
                    height: 40
                    color: "transparent"

                    Text {
                        leftPadding: 22
                        font.pixelSize: 24
                        font.family: "Rubik"
                        anchors.left: parent.left
                        text: "О приложении"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Navigate to: О приложении");
                            drawer.close();
                        }
                    }
                }
            }

            Item {
                width: parent.width
                height: 40

                Rectangle {
                    width: parent.width
                    height: 40
                    color: "transparent"

                    Text {
                        leftPadding: 22
                        font.pixelSize: 24
                        font.family: "Rubik"
                        anchors.left: parent.left
                        text: "Настройки"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Navigate to: Настройки");
                            drawer.close();
                        }
                    }
                }
            }

            Item {
                Layout.fillHeight: true
            }

            Item {
                width: parent.width
                height: 40

                Rectangle {
                    width: parent.width
                    height: 40
                    color: "transparent"

                    Text {
                        font.pixelSize: 16
                        color: "#FF0000"
                        font.family: "Rubik"
                        anchors.centerIn: parent
                        text: "Удалить учётную запись"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Delete account clicked!");
                            drawer.close();
                        }
                    }
                }
            }
        }
    }

    Popup {
        id: premiumPopup
        width: parent.width
        height: 932
        modal: true

        Rectangle {
            width: parent.width
            height: parent.height
            color: "#606FF1"

            Image {
                source: "qrc:/images/header/backArrowIcon.svg"
                width: 20
                height: 20
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: 10
                anchors.topMargin: 10
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        premiumPopup.close();
                    }
                }

            }

            ColumnLayout {
                Image {
                    source: "qrc:/images/header/headerPremiumBackground.svg"
                    width: 205
                    height: 122
                    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                    Layout.topMargin: 20
                    Layout.leftMargin: 25
                }
                Text {
                    text: "Премиум подписка"
                    font.family: "Roboto Mono"
                    font.pixelSize: 36
                    color: "white"
                    Layout.alignment: Qt.AlignHCenter
                    Layout.leftMargin: 35
                    Layout.topMargin: 10
                    font.bold: true
                }
            }

            Rectangle {
                width: parent.width
                height: parent.height - 220
                color: "white"
                anchors.bottom: parent.bottom

                ColumnLayout {
                    spacing: 20
                    anchors.fill: parent

                    RowLayout {
                        Layout.fillWidth: true
                        spacing: 10
                        Layout.topMargin: 20
                        Layout.leftMargin: 10
                        Layout.rightMargin: 10

                        Image {
                            source: "qrc:/images/header/uploadFileAdIcon.svg"
                            width: 30
                            height: 30
                        }

                        Rectangle {
                            width: 320
                            color: "transparent"
                            Layout.bottomMargin: 30
                            Text {
                                anchors.fill: parent
                                text: "Увеличенный размер файлов загрузки до 4GB"
                                font.family: "Rubik"
                                font.pixelSize: 16
                                wrapMode: Text.WordWrap
                            }
                        }

                        Image {
                            source: "qrc:/images/header/continueIcon.svg"
                            width: 20
                            height: 20
                        }
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        spacing: 10
                        Layout.leftMargin: 10
                        Layout.rightMargin: 10

                        Image {
                            source: "qrc:/images/header/adBlockAdIcon.svg"
                            width: 30
                            height: 30
                        }

                        Rectangle {
                            width: 320
                            color: "transparent"
                            Layout.bottomMargin: 30
                            Text {
                                anchors.fill: parent
                                text: "Никакой рекламы"
                                font.family: "Rubik"
                                font.pixelSize: 16
                                wrapMode: Text.WordWrap
                            }
                        }

                        Image {
                            source: "qrc:/images/header/continueIcon.svg"
                            width: 20
                            height: 20
                        }
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        spacing: 10
                        Layout.leftMargin: 10
                        Layout.rightMargin: 10

                        Image {
                            source: "qrc:/images/header/yourStyleAdIcon.svg"
                            width: 30
                            height: 30
                        }

                        Rectangle {
                            width: 320
                            color: "transparent"
                            Layout.bottomMargin: 30
                            Text {
                                anchors.fill: parent
                                text: "Дополнительные возможности для оформления приложения"
                                font.family: "Rubik"
                                font.pixelSize: 16
                                wrapMode: Text.WordWrap
                            }
                        }

                        Image {
                            source: "qrc:/images/header/continueIcon.svg"
                            width: 20
                            height: 20
                        }
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        spacing: 10
                        Layout.leftMargin: 10
                        Layout.rightMargin: 10

                        Image {
                            source: "qrc:/images/header/fasterSpeedAdIcon.svg"
                            width: 30
                            height: 30
                        }

                        Rectangle {
                            width: 320
                            color: "transparent"
                            Layout.bottomMargin: 30
                            Text {
                                anchors.fill: parent
                                text: "Повышенная скорость загрузки файлов"
                                font.family: "Rubik"
                                font.pixelSize: 16
                                wrapMode: Text.WordWrap
                            }
                        }

                        Image {
                            source: "qrc:/images/header/continueIcon.svg"
                            width: 20
                            height: 20
                        }
                    }
                    Rectangle {
                        width: 320
                        color: "transparent"
                        Layout.alignment: Qt.AlignHCenter
                        Text {

                            text: "А также множество других функций, которые ожидают Вас в будущих обновлениях!"
                            font.family: "Rubik"
                            font.pixelSize: 20
                            color: "#606FF1"
                            font.bold: true
                            anchors.fill: parent
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }

                    Item {
                        Layout.fillHeight: true
                    }

                    Rectangle {
                        Layout.fillWidth: true
                        Layout.leftMargin: 25
                        Layout.rightMargin: 25
                        radius: 10
                        height: 50
                        color: "#606FF1"
                        Text {
                            anchors.centerIn: parent
                            text: "199 руб/мес"
                            color: "white"
                            font.family: "Rubik"
                            font.pixelSize: 20
                            font.bold: true
                        }
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                        Layout.bottomMargin: 10
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                premiumPopup.close();
                            }
                        }
                    }
                }
            }
        }
    }

}
