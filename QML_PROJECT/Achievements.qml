import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Page {
    width: 430
    height: 932

    header: PageHeader {}

    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        Rectangle {
            Layout.topMargin: 10
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Мои достижения"
                        font.pixelSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
            }
        }

        Rectangle {
            Layout.topMargin: 20
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                spacing: 5
                Item {
                    Layout.fillWidth: true
                }
                Rectangle {
                    width: 162
                    height: 64
                    color: "#606FF1"
                    radius: 10
                    border.width: 1
                    border.color: "black"
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Награды"
                        color: "white"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
                Rectangle {
                    width: 162
                    height: 64
                    color: "#CCDCF3"
                    radius: 10
                    border.width: 1
                    border.color: "black"
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Статистика"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: windowLoader.source = "AchievementStatistics.qml"
                    }
                }
                Item {
                    Layout.fillWidth: true
                }
            }
        }

        Rectangle {
            Layout.topMargin: 40
            Layout.leftMargin: 16
            Layout.rightMargin: 16
            width: 400
            height: 67
            color: "#CCDCF3"
            radius: 10
            RowLayout {
                anchors.fill: parent
                Image {
                    Layout.leftMargin: 15
                    source: "qrc:/images/main/timer.svg"
                    width: 40
                    height: 40
                }
                Rectangle {
                    width: 350
                    Text {
                        text: "За месяц ты не просрочил ни одного дедлайна!"
                        font.pixelSize: 16
                        wrapMode: Text.WordWrap
                        width: parent.width - 20
                        font.family: "Rubik"
                        anchors.fill: parent
                        anchors.leftMargin: 20
                        anchors.topMargin: -20
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
            }
        }

        Rectangle {
            Layout.topMargin: 20
            Layout.leftMargin: 16
            Layout.rightMargin: 16
            width: 400
            height: 67
            color: "#CCDCF3"
            radius: 10
            RowLayout {
                anchors.fill: parent
                Image {
                    Layout.leftMargin: 15
                    source: "qrc:/images/main/achievement.svg"
                    width: 40
                    height: 40
                }
                Rectangle {
                    width: 350
                    Text {
                        text: "Ты каждый день выполнял задачи в течение недели!"
                        font.pixelSize: 16
                        wrapMode: Text.WordWrap
                        width: parent.width - 20
                        font.family: "Rubik"
                        anchors.fill: parent
                        anchors.leftMargin: 20
                        anchors.topMargin: -20
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
            }
        }

        Rectangle {
            Layout.topMargin: 20
            Layout.rightMargin: 16
            width: 200
            height: 50
            color: "#606FF1"
            radius: 10
            Layout.alignment: Qt.AlignRight
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 150
                    Text {
                        text: "Прогресс наград"
                        font.pixelSize: 16
                        width: parent.width - 20
                        font.family: "Rubik"
                        anchors.fill: parent
                        color: "white"
                        anchors.topMargin: -10
                        anchors.leftMargin: 15
                    }
                }
                Image {
//                    Layout.leftMargin: 15
                    Layout.rightMargin: 15
                    source: "qrc:/images/main/progress.svg"
                    width: 23
                    height: 23
                }
            }
        }

        Item {
            Layout.fillHeight: true
        }

        Rectangle {
            Layout.preferredWidth: 20
            Layout.preferredHeight: 20
            color: "transparent"
            Layout.alignment: Qt.AlignRight | Qt.AlignBottom
            Layout.bottomMargin: -5

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/closeAdIcon.svg"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Banner closed!")
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            Layout.alignment: Qt.AlignBottom
            Layout.bottomMargin: 18

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/adBanner.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Banner clicked!")
                    }
                }
            }
        }
    }

    footer: PageFooter {}

 }


