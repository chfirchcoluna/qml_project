import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Page {
    width: 430
    height: 932

    header: PageHeader {}

    Rectangle {
        width: parent.width
        height: parent.height
        color: "white"
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        spacing: 0

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 125
            color: "transparent"
            Layout.topMargin: 18
            Layout.leftMargin: 50
            Layout.rightMargin: 50
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/updateBanner.png"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Banner clicked!")
                }
            }
        }

        Image {
            Layout.preferredWidth: 32
            Layout.preferredHeight: 32
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.leftMargin: 50
            Layout.topMargin: 30

            source: "qrc:/images/main/calendarIcon.svg"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    calendarPopup.open()
                    console.log("Calendar clicked!")
                }
            }
        }

        SwipeView {
            id: view
            spacing: 0
            currentIndex: 1
            orientation: Qt.Horizontal
            Layout.preferredWidth: 351
            Layout.preferredHeight: 386
            Layout.leftMargin: 40
            Layout.rightMargin: 40
            Layout.topMargin: 30

            DayCard {}
            DayCard {}
            DayCard {}

            Timer {
                id: initialTimer
                interval: 0
                repeat: false
                onTriggered: {
                    view.addPage();
                    view.addPage();
                    view.addPage();
                }
            }

            Component.onCompleted: {
                initialTimer.start();
            }

            Timer {
                id: delayTimer
                interval: 1000
                repeat: false
                onTriggered: {
                    if (view.currentIndex === 0) {
                        view.addPage(true);
                    } else if (view.currentIndex === 2) {
                        view.addPage();
                    }
                }
            }

            onCurrentIndexChanged: {
                delayTimer.restart();
            }

            function addPage(left) {
                var newPage = Qt.createComponent("DayCard.qml").createObject(view);
                var currentCard = view.itemAt(view.currentIndex);
                var currentDate = currentCard.comDate;
                if (left) {
                    var newDate = view.updateDate(currentDate, false);
                    currentCard.updateComDate(newDate);
                    currentCard.comText = currentCard.comDateToString(newDate);
                    newPage.updateComDate(newDate);
                    newPage.comText = newPage.comDateToString(newDate);
                    view.moveItem(3, 0);
                    view.removeItem(view.itemAt(3));
                } else {
                    var newDate1 = view.updateDate(currentDate, true);
                    currentCard.updateComDate(newDate1);
                    currentCard.comText = currentCard.comDateToString(newDate1);
                    newPage.updateComDate(newDate1);
                    newPage.comText = newPage.comDateToString(newDate1);
                    view.removeItem(view.itemAt(0));
                }
            }

            function updateDate(currentDate, isNextDay) {
                if (isNextDay) {
                    currentDate.setDate(currentDate.getDate() + 1);
                } else {
                    currentDate.setDate(currentDate.getDate() - 1);
                }
                return currentDate;
            }
        }

        Item {
            Layout.fillHeight: true
        }

        Rectangle {
            Layout.preferredWidth: 20
            Layout.preferredHeight: 20
            color: "transparent"
            Layout.alignment: Qt.AlignRight | Qt.AlignBottom
            Layout.bottomMargin: 0

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/closeAdIcon.svg"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Banner closed!")
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            Layout.alignment: Qt.AlignBottom
            Layout.bottomMargin: 18

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/adBanner.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Banner clicked!")
                    }
                }
            }
        }
    }

    Popup {
        id: calendarPopup
        width: 430 - 60
        height: 932 - 60
        anchors.centerIn: parent
        opacity: 0
        modal: true
        Overlay.modal: Rectangle {
            color: "#1E1E1E00"
        }
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onOpened: {
            calendarPopup.opacity = 1;
        }

        onClosed: {
            calendarPopup.opacity = 0;
        }

        contentItem: Rectangle {
            width: parent.width
            height: parent.height
            anchors.centerIn: parent
            anchors.fill: parent

            ColumnLayout {
                width: parent.width
                height: parent.height
                spacing: 0
                Rectangle {
                    Layout.alignment: Qt.AlignTop
                    width: parent.width
                    height: 50
                    color: "#606FF1"

                    RowLayout {
                        anchors.fill: parent
                        spacing: 0

                        Text {
                            Layout.leftMargin: 55
                            Layout.alignment: Qt.AlignHCenter
                            text: "Календарь"
                            color: "white"
                            font.pixelSize: 24
                            font.bold: true
                        }

                        Image {
                            Layout.alignment: Qt.AlignRight
                            Layout.rightMargin: 15
                            source: "qrc:/images/main/closeIcon.svg"
                            width: 20
                            height: 20
                            MouseArea {
                                anchors.fill: parent
                                onClicked: calendarPopup.close()
                            }
                        }
                    }
                }

                Rectangle {
                    width: parent.width - 40
                    height: 90
                    color: "#FFB672"
                    radius: 10
                    Layout.topMargin: 20
                    Layout.leftMargin: 20
                    Layout.rightMargin: 20

                    RowLayout {
                        anchors.fill: parent
                        spacing: 0

                        Image {
                            Layout.alignment: Qt.AlignLeft
                            source: "qrc:/images/main/leftArrow.svg"
                            width: 20
                            height: 20
                            Layout.leftMargin: 15
                        }

                        Item {
                            Layout.fillWidth: true
                        }

                        Text {
                            text: "Октябрь 2023"
                            font.pixelSize: 24
                            font.family: "Rubik"
                            font.weight: Font.Bold
                            color: "white"
                        }

                        Item {
                            Layout.fillWidth: true
                        }

                        Image {
                            Layout.alignment: Qt.AlignRight
                            source: "qrc:/images/main/rightArrow.svg"
                            width: 20
                            height: 20
                            Layout.rightMargin: 15
                        }
                    }
                }

                ColumnLayout {
                    Layout.alignment: Qt.AlignBottom
                    width: parent.width
                    spacing: 5
                    Layout.topMargin: 20
                    Layout.leftMargin: 20
                    Layout.rightMargin: 20
                    RowLayout {
                        width: parent.width
                        spacing: 5
                        CalendarCell {comText: "пн"; comBold: true}
                        CalendarCell {comText: "вт"; comBold: true}
                        CalendarCell {comText: "ср"; comBold: true}
                        CalendarCell {comText: "чт"; comBold: true}
                        CalendarCell {comText: "пт"; comBold: true}
                        CalendarCell {comText: "сб"; comBold: true}
                        CalendarCell {comText: "вс"; comBold: true}
                    }
                    RowLayout {
                        width: parent.width
                        spacing: 5
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: "1"}
                    }
                    RowLayout {
                        width: parent.width
                        spacing: 5
                        CalendarCell {comText: "2"}
                        CalendarCell {comText: "3"}
                        CalendarCell {comText: "4"}
                        CalendarCell {comText: "5"}
                        CalendarCell {comText: "6"}
                        CalendarCell {comText: "7"}
                        CalendarCell {comText: "8"}
                    }
                    RowLayout {
                        width: parent.width
                        spacing: 5
                        Rectangle {
                            Rectangle {
                                radius: 20
                                width: 30
                                height: 30
                                border.width: 2
                                border.color: "black"
                                anchors.centerIn: parent
                            }

                            width: (parent.width - 30) / 8
                            height: 30
                            Text {
                                font.pixelSize: 20
                                color: "#606FF1"
                                anchors.centerIn: parent
                                text: "9"
                            }
                        }
                        CalendarCell {comText: "10"}
                        CalendarCell {comText: "11"}
                        CalendarCell {comText: "12"}
                        CalendarCell {comText: "13"}
                        CalendarCell {comText: "14"}
                        CalendarCell {comText: "15"}
                    }
                    RowLayout {
                        width: parent.width
                        spacing: 5
                        CalendarCell {comText: "16"}
                        CalendarCell {comText: "17"}
                        CalendarCell {comText: "18"}
                        CalendarCell {comText: "19"}
                        CalendarCell {comText: "20"}
                        CalendarCell {comText: "21"}
                        CalendarCell {comText: "22"}
                    }
                    RowLayout {
                        width: parent.width
                        spacing: 5
                        CalendarCell {comText: "23"}
                        CalendarCell {comText: "24"}
                        Rectangle {
                            Rectangle {
                                anchors.bottom: parent.bottom

                                radius: 10
                                width: 6
                                height: 6
                                color: "black"
                                anchors.horizontalCenter: parent.horizontalCenter

                            }
                            width: (parent.width - 30) / 8
                            height: 30
                            Text {
                                font.pixelSize: 20
                                color: "#606FF1"
                                anchors.centerIn: parent
                                text: "25"
                            }
                        }
                        CalendarCell {comText: "26"}
                        CalendarCell {comText: "27"}
                        CalendarCell {comText: "28"}
                        CalendarCell {comText: "29"}
                    }
                    RowLayout {
                        width: parent.width
                        spacing: 5
                        CalendarCell {comText: "30"}
                        CalendarCell {comText: "31"}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                        CalendarCell {comText: ""}
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Rectangle {
                    //Layout.fillWidth: true
                    Layout.preferredHeight: 59
                    Layout.preferredWidth: 59
                    Layout.alignment: Qt.AlignBottom | Qt.AlignHCenter
                    Layout.bottomMargin: 50
                    color: "#606FF1"
                    radius: 100

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Plus button clicked!")
                        }
                    }

                    Text {
                        anchors.centerIn: parent
                        text: "+"
                        color: "white"
                        font.pointSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
            }
        }
    }



    footer: PageFooter {}
}
