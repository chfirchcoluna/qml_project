import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Rectangle {
    color: "#CCDCF3"
    width: 220
    height: 155
    border.color: "#606FF1"
    radius: 20
    border.width: 4
    ColumnLayout {
        width: parent.width
        anchors.fill: parent
        RowLayout {
            width: parent.width
            Rectangle {
                Layout.leftMargin: 10
                Layout.bottomMargin: 20
                width: 125
                Text {
                    text: "Программирование на Python для начинающих"
                    wrapMode: Text.WordWrap
                    width: parent.width
                }
            }
            Item {
                Layout.fillWidth: true
            }

            Image {
                Layout.topMargin: 15
                Layout.rightMargin: 10
                source: "qrc:/images/main/courseImage0.png"
            }
        }
        Text {
            Layout.leftMargin: 10
            text: "ИГУ"
            font.pixelSize: 10
            font.family: "Rubik"
        }

        Item {
            Layout.fillHeight: true
        }

        RowLayout {
            width: parent.width
            Layout.bottomMargin: 15
            Text {
                Layout.leftMargin: 10
                text: "1390₽"
                color: "#606FF1"
                font.pixelSize: 12
                font.bold: true
                font.family: "Rubik"
            }
            Item {
                Layout.fillWidth: true
            }
            Image {
                source: "qrc:/images/main/time.svg"
            }
            Text {
                text: "15ч"
                font.pixelSize: 12
                font.family: "Rubik"
            }
            Image {
                source: "qrc:/images/main/star.svg"
            }
            Text {
                Layout.rightMargin: 10
                text: "5"
                font.pixelSize: 12
                font.family: "Rubik"
            }
        }
    }
}
