import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Page {
    width: 430
    height: 932

    header: PageHeader {}

    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        Rectangle {
            Layout.topMargin: 10
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Мои достижения"
                        font.pixelSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
            }
        }

        Rectangle {
            Layout.topMargin: 20
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                spacing: 5
                Item {
                    Layout.fillWidth: true
                }
                Rectangle {
                    width: 162
                    height: 64
                    color: "#CCDCF3"
                    radius: 10
                    border.width: 1
                    border.color: "black"
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Награды"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: windowLoader.source = "Achievements.qml"
                    }
                }
                Rectangle {
                    width: 162
                    height: 64
                    color: "#606FF1"
                    radius: 10
                    border.width: 1
                    border.color: "black"
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Статистика"
                        color: "white"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
                Item {
                    Layout.fillWidth: true
                }
            }
        }

        Rectangle {
            Layout.topMargin: 40
            Layout.leftMargin: 16
            Layout.rightMargin: 16
            width: 400
            height: 50
            radius: 10
            RowLayout {
                anchors.fill: parent
                Item {
                    Layout.fillWidth: true
                }

                Rectangle {
                    width: 220
                    Text {
                        text: "По заданиям в месяц"
                        font.bold: true
                        font.pixelSize: 20
                        width: parent.width - 20
                        font.family: "Rubik"
                        anchors.fill: parent
                        anchors.topMargin: -12
                    }
                }
                Image {
                    Layout.leftMargin: 0
                    source: "qrc:/images/main/arrowDown.svg"
                    width: 12
                    height: 18
                }
                Item {
                    Layout.fillWidth: true
                }
            }
        }

        Image {
            Layout.leftMargin: 9
            source: "qrc:/images/main/graphic.png"
            width: 412
            height: 258
        }

        RowLayout {
            width: parent.width
            height: 50
            Layout.leftMargin: 15
            Layout.rightMargin: 9
            Text {
                Layout.topMargin: 30
                text: "21.10.23 -"
                font.weight: Font.Bold
                color: "#606FF1"
                font.pixelSize: 16
                font.family: "Rubik"
            }
            Rectangle {
                width: 330
                Text {
                    text: "В этот день ты сделал рекорд по выполненным заданиям за день (20)!"
                    font.pixelSize: 16
                    wrapMode: Text.WordWrap
                    width: parent.width
                    font.family: "Rubik"
                }
            }
        }

        Item {
            Layout.fillHeight: true
        }

        Rectangle {
            Layout.preferredWidth: 20
            Layout.preferredHeight: 20
            color: "transparent"
            Layout.alignment: Qt.AlignRight | Qt.AlignBottom
            Layout.bottomMargin: -5

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/closeAdIcon.svg"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Banner closed!")
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            Layout.alignment: Qt.AlignBottom
            Layout.bottomMargin: 18

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/adBanner.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Banner clicked!")
                    }
                }
            }
        }
    }

    footer: PageFooter {}

 }


