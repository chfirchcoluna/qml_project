import QtQuick 2.15
import QtQuick.Layouts 1.15

Item {
    id: deleg
    property string delegSubject: "Subject"
    property string delegTime: "Today"
    property string delegColorText: "black"
    property string delegColorBack: "white"
    width: parent.width
    height: 40

    RowLayout {
        spacing: 10
        anchors.fill: parent

        Rectangle {
            Layout.leftMargin: 5
            width: 95
            radius: 10
            height: 30
            color: delegColorBack
            border.width: 2
            border.color: "white"

            Text {
                anchors.centerIn: parent
                //font.pointSize: 13
                font.family: "Rubik"
                font.weight: Font.Bold
                color: delegColorText
                text: delegTime
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.rightMargin: 5
            radius: 10
            height: parent.height
            color: delegColorBack
            border.width: 2
            border.color: "white"

            Text {
                anchors.centerIn: parent
                font.pointSize: 19
                font.family: "Rubik"
                font.weight: Font.Bold
                color: delegColorText
                text: delegSubject
            }
        }
    }
}
