import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Page {

    Rectangle {
        width: parent.width
        height: parent.height
        color: "white"
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        Rectangle {
            width: parent.width
            height: 40

            RowLayout {
                anchors.fill: parent
                spacing: 10

                Image {
                    Layout.topMargin: 10
                    source: "qrc:/images/header/backArrowNegativeIcon.svg"
                    width: 20
                    height: 20
                    Layout.leftMargin: 15
                    MouseArea {
                        anchors.fill: parent
                        onClicked: windowLoader.source = "SubjectExample.qml"
                    }
                }

                Item {
                    Layout.fillWidth: true
                }

                Image {
                    Layout.topMargin: 10
                    source: "qrc:/images/header/notificationsSettings.svg"
                    width: 28
                    height: 28
                    Layout.rightMargin: 15
                    MouseArea {
                        anchors.fill: parent
                    }
                }
            }
        }

        Rectangle {
            Layout.topMargin: 30
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 380
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Домашняя контрольная по теории вероятности"
                        color: "#606FF1"
                        font.pointSize: 22
                        font.family: "Rubik"
                        wrapMode: Text.WordWrap
                        font.weight: Font.Bold
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

            }
        }

        Rectangle {
            Layout.topMargin: 30
            width: parent.width
            height: 10
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: parent.width - 270
                    height: 30
                    Layout.alignment: Qt.AlignHCenter
                    border.width: 2
                    radius: 10
                    RowLayout {
                        Text {
                            Layout.topMargin: 5
                            Layout.leftMargin: 10
                            text: "Дедлайн:"
                            font.pixelSize: 16
                            font.family: "Rubik"
                            font.weight: Font.Bold
                        }
                        Text {
                            Layout.topMargin: 5
                            text: "25.10.23"
                            color: "#FF0000"
                            font.pixelSize: 16
                            font.family: "Rubik"
                            font.weight: Font.Bold
                        }
                    }
                }

            }
        }

        RowLayout {
            Layout.topMargin: 30
            Layout.leftMargin: 15
            width: parent.width
            height: 30
            Text {
                text: "Статус: "
                color: "#606FF1"
                font.pixelSize: 20
                font.family: "Rubik"
                font.weight: Font.Bold
            }
            Text {
                text: "Просрочено"
                color: "#FF0000"
                font.pixelSize: 16
                font.family: "Rubik"
                font.weight: Font.Bold
            }
        }

        RowLayout {
            Layout.topMargin: 20
            Layout.leftMargin: 15
            width: parent.width
            height: 30
            Text {
                text: "Предмет: "
                color: "#606FF1"
                font.pixelSize: 20
                font.family: "Rubik"
                font.weight: Font.Bold
            }
            Text {
                text: "Математика"
                color: "#0019FF"
                font.pixelSize: 16
                font.family: "Rubik"
                font.weight: Font.Bold
            }
        }

        Text {
            Layout.topMargin: 20
            Layout.leftMargin: 15
            text: "Описание"
            color: "#606FF1"
            font.pixelSize: 20
            font.family: "Rubik"
            font.weight: Font.Bold
        }

        Rectangle {
            Layout.topMargin: 5
            width: 400
            height: 150
            color: "#CCDCF3"
            radius: 10
            Layout.leftMargin: 15
            Text {
                padding: 10
                text: "Повседневная практика показывает, что рамки и место обучения кадров требует от нас анализа всесторонне сбалансированных нововведений! Практический опыт показывает, что дальнейшее развитие различных форм деятельности "
                font.pixelSize: 16
                font.family: "Rubik"
                width: 350
                wrapMode: Text.WordWrap
            }
        }

        Text {
            Layout.topMargin: 20
            Layout.leftMargin: 15
            text: "Промежуток времени"
            color: "#606FF1"
            font.pixelSize: 20
            font.family: "Rubik"
            font.weight: Font.Bold
        }

        Rectangle {
            Layout.topMargin: 5
            width: 185
            height: 45
            color: "#CCDCF3"
            radius: 10
            Layout.leftMargin: 15
            Text {
                anchors.centerIn: parent
                padding: 10
                text: "10:10 - 11:40"
                font.pixelSize: 16
                font.family: "Rubik"
            }
        }

        Text {
            Layout.topMargin: 20
            Layout.leftMargin: 15
            text: "Файлы"
            color: "#606FF1"
            font.pixelSize: 20
            font.family: "Rubik"
            font.weight: Font.Bold
        }

        Text {
            Layout.leftMargin: 15
            text: "1. math_kr1.txt"
            font.pixelSize: 20
            font.family: "Rubik"
            font.weight: Font.Bold
        }

        Text {
            Layout.leftMargin: 15
            text: "2. lection.pdf"
            font.pixelSize: 20
            font.family: "Rubik"
            font.weight: Font.Bold
        }

        Item {
            Layout.fillHeight: true
        }
    }

    footer: PageFooter {}
}
