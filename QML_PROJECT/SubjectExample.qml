import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt5Compat.GraphicalEffects


Page {

    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        Rectangle {
            width: parent.width
            height: 40

            RowLayout {
                anchors.fill: parent
                spacing: 10

                Image {
                    Layout.topMargin: 10
                    source: "qrc:/images/header/backArrowNegativeIcon.svg"
                    width: 20
                    height: 20
                    Layout.leftMargin: 15
                    MouseArea {
                        anchors.fill: parent
                        onClicked: windowLoader.source = "Tasks.qml"
                    }
                }

                Item {
                    Layout.fillWidth: true
                }

                Image {
                    Layout.topMargin: 10
                    source: "qrc:/images/header/notificationsSettings.svg"
                    width: 28
                    height: 28
                    Layout.rightMargin: 15
                    MouseArea {
                        anchors.fill: parent
                    }
                }
            }
        }

        Rectangle {
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Математика"
                        color: "#0019FF"
                        font.pointSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }

            }
        }

        Rectangle {
            Layout.fillWidth: true
            height: 30
            width: 430
            RowLayout {
                anchors.fill: parent
                Image {
                    Layout.alignment: Qt.AlignHCenter
                    Layout.rightMargin: 100
                    source: "qrc:/images/main/mathematic.svg"
                }
            }
        }


        Rectangle {
            width: parent.width
            Layout.leftMargin: 50
            Layout.topMargin: 30
            Layout.rightMargin: 50
            height: 40
            RowLayout {
                Layout.fillWidth: true
                spacing: 20

                Rectangle {
                    width: 115
                    height: 40
                    Rectangle {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 15
                        width: 70
                        height: 5
                        color: "#606FF1"
                        radius: 10
                    }

                    Text {
                        text: "Задания"
                        font.pixelSize: 16
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }

                Rectangle {
                    width: 105
                    height: 40
                    Text {
                        text: "Файлы"
                        font.pixelSize: 16
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }

                Rectangle {
                    width: 100
                    height: 40
                    Text {
                        text: "Обучение"
                        font.pixelSize: 16
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
            }
        }

        Rectangle {
            width: parent.width
            height: 1
            color: "#B0C4DE"
            layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 3
                    verticalOffset: 3
                    radius: 8
                    samples: 17
                    color: "#80000000"
                }
        }

        Rectangle {
            MouseArea {
                anchors.fill: parent
                onClicked: windowLoader.source = "TaskExample.qml"
            }

            Layout.bottomMargin: 10
            width: parent.width
            height: 20
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        text: "Домашняя контрольная по теории вероятности"
                        font.pixelSize: 16
                        font.family: "Rubik"
                        color: "#606FF1"
                    }
                }

            }
        }

        Rectangle {
            MouseArea {
                anchors.fill: parent
                onClicked: windowLoader.source = "TaskExample.qml"
            }
            Layout.bottomMargin: 10
            width: parent.width
            height: 20
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        text: "25.10.23"
                        font.pixelSize: 14
                        font.family: "Rubik"
                        color: "#FF0000"
                    }
                }

            }
        }

        Rectangle {
            width: parent.width - 40
            height: 1
            color: "#B0C4DE"
            Layout.leftMargin: 20
            Layout.rightMargin: 20
        }

        Rectangle {
            Layout.bottomMargin: 10
            width: parent.width
            height: 20
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        text: "Доделать работу на паре"
                        font.pixelSize: 16
                        font.family: "Rubik"
                        color: "#606FF1"
                    }
                }

            }
        }

        Rectangle {
            Layout.bottomMargin: 10
            width: parent.width
            height: 20
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        text: "27.10.23"
                        font.pixelSize: 14
                        font.family: "Rubik"
                        color: "black"
                    }
                }

            }
        }

        Rectangle {
            width: parent.width - 40
            height: 1
            color: "#B0C4DE"
            Layout.leftMargin: 20
            Layout.rightMargin: 20
        }

        Rectangle {
            Layout.bottomMargin: 10
            width: parent.width
            height: 20
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        text: "Теория графов"
                        font.pixelSize: 16
                        font.family: "Rubik"
                        color: "#606FF1"
                    }
                }

            }
        }

        Rectangle {
            Layout.bottomMargin: 10
            width: parent.width
            height: 20
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        text: "29.10.23"
                        font.pixelSize: 14
                        font.family: "Rubik"
                        color: "black"
                    }
                }

            }
        }

        Rectangle {
            width: parent.width - 40
            height: 1
            color: "#B0C4DE"
            Layout.leftMargin: 20
            Layout.rightMargin: 20
        }

        Item {
            Layout.fillHeight: true
        }

        Rectangle {
            //Layout.fillWidth: true
            Layout.preferredHeight: 59
            Layout.preferredWidth: 59
            Layout.rightMargin: 115
            Layout.alignment: Qt.AlignBottom | Qt.AlignRight
            Layout.bottomMargin: 5
            color: "#606FF1"
            radius: 100

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Plus button clicked!")
                }
            }

            Text {
                anchors.centerIn: parent
                text: "+"
                color: "white"
                font.pointSize: 24
                font.family: "Rubik"
                font.weight: Font.Bold
            }
        }

        Rectangle {
            width: 20
            height: 20
            color: "transparent"
            Layout.alignment:  Qt.AlignBottom | Qt.AlignRight
            Layout.rightMargin: 100
            Layout.bottomMargin: -5

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/closeAdIcon.svg"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Banner closed!")
                }
            }
        }

        Rectangle {
            width: 430
            Layout.preferredHeight: 100
            Layout.alignment: Qt.AlignBottom
            Layout.bottomMargin: 18

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/adBanner.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Banner clicked!")
                    }
                }
            }
        }
    }

    footer: PageFooter {}
}
