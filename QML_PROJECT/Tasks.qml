import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Page {
    width: 430
    height: 932
    header: PageHeader {}

    Rectangle {
        width: parent.width
        height: parent.height
        color: "white"
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        Rectangle {
            Layout.topMargin: 10
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                Rectangle {
                    width: 264
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Задачи"
                        font.pixelSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
            }
        }

        Rectangle {
            Layout.topMargin: 20
            width: parent.width
            height: 40
            RowLayout {
                anchors.fill: parent
                spacing: 5
                Item {
                    Layout.fillWidth: true
                }
                Rectangle {
                    width: 162
                    height: 64
                    color: "#606FF1"
                    radius: 10
                    border.width: 1
                    border.color: "black"
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Категории"
                        color: "white"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
                Rectangle {
                    width: 162
                    height: 64
                    color: "#CCDCF3"
                    radius: 10
                    border.width: 1
                    border.color: "black"
                    Layout.alignment: Qt.AlignHCenter
                    Text {
                        anchors.centerIn: parent
                        text: "Все"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                    MouseArea {
                        anchors.fill: parent
                    }
                }
                Item {
                    Layout.fillWidth: true
                }
            }
        }

        RowLayout {
            Layout.topMargin: 50
            Item {
                Layout.fillWidth: true
            }
            Image {
                source: "qrc:/images/main/order.svg"
                Layout.rightMargin: 15
            }
            Image {
                source: "qrc:/images/main/filters.svg"
                Layout.rightMargin: 15
                MouseArea {
                    anchors.fill: parent
                    onClicked: filters.open()
                }
            }
        }

        RowLayout {
            width: parent.width
            height: 50
            MouseArea {
                width: parent.width
                height: parent.height
                onClicked: windowLoader.source = "SubjectExample.qml"
            }

            Layout.topMargin: 5
            Layout.fillWidth: true
            Item {
                Layout.fillWidth: true
            }
            Image {
                source: "qrc:/images/main/mathematic.svg"
            }
            Item {
                Layout.fillWidth: true
            }
            Rectangle {
                width: 257
                height: 40
                border.width: 1
                radius: 10
                Text {
                    anchors.centerIn: parent
                    text: "Математика"
                    color: "#0019FF"
                    font.pixelSize: 20
                    font.family: "Rubik"
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Rectangle {
                width: 73
                height: 40
                color: "#CCDCF3"
                radius: 10
                Text {
                    anchors.centerIn: parent
                    text: "3"
                    font.pixelSize: 20
                    font.family: "Rubik"
                }
            }
            Item {
                Layout.fillWidth: true
            }
        }

        RowLayout {
            Layout.topMargin: 5
            Layout.fillWidth: true
            Item {
                Layout.fillWidth: true
            }
            Image {
                source: "qrc:/images/main/economic.svg"
            }
            Item {
                Layout.fillWidth: true
            }
            Rectangle {
                width: 257
                height: 40
                border.width: 1
                radius: 10
                Text {
                    anchors.centerIn: parent
                    text: "Экономика"
                    color: "#F1C060"
                    font.pixelSize: 20
                    font.family: "Rubik"
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Rectangle {
                width: 73
                height: 40
                color: "#CCDCF3"
                radius: 10
                Text {
                    anchors.centerIn: parent
                    text: "1"
                    font.pixelSize: 20
                    font.family: "Rubik"
                }
            }
            Item {
                Layout.fillWidth: true
            }
        }

        RowLayout {
            Layout.topMargin: 5
            Layout.fillWidth: true
            Item {
                Layout.fillWidth: true
            }
            Image {
                source: "qrc:/images/main/design.svg"
            }
            Item {
                Layout.fillWidth: true
            }
            Rectangle {
                width: 257
                height: 40
                border.width: 1
                radius: 10
                Text {
                    anchors.centerIn: parent
                    text: "Дизайн"
                    color: "#FF00C7"
                    font.pixelSize: 20
                    font.family: "Rubik"
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Rectangle {
                width: 73
                height: 40
                color: "#CCDCF3"
                radius: 10
                Text {
                    anchors.centerIn: parent
                    text: "5"
                    font.pixelSize: 20
                    font.family: "Rubik"
                }
            }
            Item {
                Layout.fillWidth: true
            }
        }

        Item {
            Layout.fillHeight: true
        }

        Rectangle {
            //Layout.fillWidth: true
            Layout.preferredHeight: 59
            Layout.preferredWidth: 59
            Layout.rightMargin: 15
            Layout.alignment: Qt.AlignBottom | Qt.AlignRight
            Layout.bottomMargin: 5
            color: "#606FF1"
            radius: 100

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Plus button clicked!")
                }
            }

            Text {
                anchors.centerIn: parent
                text: "+"
                color: "white"
                font.pointSize: 24
                font.family: "Rubik"
                font.weight: Font.Bold
            }
        }

        Rectangle {
            Layout.preferredWidth: 20
            Layout.preferredHeight: 20
            color: "transparent"
            Layout.alignment: Qt.AlignRight | Qt.AlignBottom
            Layout.bottomMargin: -5

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/closeAdIcon.svg"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Banner closed!")
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            Layout.alignment: Qt.AlignBottom
            Layout.bottomMargin: 18

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/adBanner.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Banner clicked!")
                    }
                }
            }
        }
    }

    Popup {
        id: filters
        width: 430
        height: 388
        topMargin: 550
        Rectangle {
            width: parent.width
            height: 388
            color: "white"
        }

        ColumnLayout {
            spacing: 15
            width: parent.width
            height: parent.height
            Rectangle {
                width: 63
                height: 5
                color: "#C1C1C1"
                Layout.alignment: Qt.AlignHCenter
                radius: 5
                MouseArea {
                    anchors.fill: parent
                    onClicked: filters.close()
                }
            }
            Text {
                text: "Фильтры"
                font.pixelSize: 24
                font.family: "Rubik"
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }
            Rectangle {
                width: 380
                height: 2
                color: "#BFBFBF"
                Layout.alignment: Qt.AlignHCenter
            }
            Text {
                text: "Преподаватель:"
                font.pixelSize: 16
                font.family: "Rubik"
                Layout.leftMargin: 15
            }
            RowLayout {
                Rectangle {
                    Layout.leftMargin: 15
                    width: 20
                    height: 20
                    border.color: "#606FF1"
                    border.width: 3
                }
                Text {
                    text: "Лутковская Е.А."
                    font.pixelSize: 16
                    font.family: "Rubik"
                    Layout.leftMargin: 15
                }
            }
            RowLayout {
                Rectangle {
                    Layout.leftMargin: 15
                    width: 20
                    height: 20
                    border.color: "#606FF1"
                    border.width: 3
                }
                Text {
                    text: "Зорина Г.Г."
                    font.pixelSize: 16
                    font.family: "Rubik"
                    Layout.leftMargin: 15
                }
            }
            RowLayout {
                Rectangle {
                    Layout.leftMargin: 15
                    width: 20
                    height: 20
                    border.color: "#606FF1"
                    border.width: 3
                }
                Text {
                    text: "Волохова С.Г."
                    font.pixelSize: 16
                    font.family: "Rubik"
                    Layout.leftMargin: 15
                }
            }
            Rectangle {
                width: 380
                height: 2
                color: "#BFBFBF"
                Layout.alignment: Qt.AlignHCenter
            }
            Text {
                text: "Упорядочить по:"
                font.pixelSize: 16
                font.family: "Rubik"
                Layout.leftMargin: 15
            }
            RowLayout {
                Rectangle {
                    Layout.leftMargin: 15
                    width: 20
                    height: 20
                    radius: 20
                    border.color: "#606FF1"
                    border.width: 3
                    Rectangle {
                        anchors.centerIn: parent
                        width: 10
                        height: 10
                        radius: 10
                        color: "#606FF1"
                    }
                }
                Text {
                    text: "Ближайшие дедлайны"
                    font.pixelSize: 16
                    font.family: "Rubik"
                    Layout.leftMargin: 15
                }
            }
            RowLayout {
                Rectangle {
                    Layout.leftMargin: 15
                    width: 20
                    height: 20
                    radius: 20
                    border.color: "#606FF1"
                    border.width: 3
                }
                Text {
                    text: "Количество заданий"
                    font.pixelSize: 16
                    font.family: "Rubik"
                    Layout.leftMargin: 15
                }
            }
            Item {
                Layout.fillHeight: true
            }
        }
    }

    footer: PageFooter {}
}
