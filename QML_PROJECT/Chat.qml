import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Page {
//    width: 430
//    height: 932

    header: PageHeader {}

    Rectangle {
        width: parent.width
        height: parent.height
        color: "white"
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        Rectangle {
            width: parent.width
            Layout.leftMargin: 50
            Layout.topMargin: 30
            Layout.rightMargin: 50
            height: 40
            RowLayout {
                Layout.fillWidth: true
                spacing: 20

                Rectangle {
                    width: 160
                    height: 40
                    Rectangle {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 12
                        width: 120
                        height: 5
                        color: "#606FF1"
                        radius: 10
                    }

                    Text {
                        text: "Мои чаты"
                        font.pixelSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }

                Rectangle {
                    width: 120
                    height: 40
                    Text {
                        text: "Поиск чатов"
                        font.pixelSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
            }
        }

        Rectangle {
            width: parent.width
            height: 1
            color: "black"
        }

        Rectangle {
            width: parent.width
            height: 75
            color: "#CCDCF3"

            MouseArea {
                anchors.fill: parent
                onClicked: chat.open()
            }

            RowLayout {
                Layout.fillWidth: true
                width: parent.width
                Image {
                    Layout.leftMargin: 10
                    Layout.topMargin: 7
                    source: "qrc:/images/main/chatProfile0.png"
                    width: 60
                    height: 60
                }
                ColumnLayout {
                    Layout.leftMargin: 10
                    Text {
                        text: "Vloodek"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        //font.weight: Font.Bold
                    }
                    Text {
                       text: "Приветствую вас, чемпион"
                       font.pixelSize: 13
                       font.family: "Rubik"
                    }
                }
                Item {
                    Layout.fillWidth: true
                }

                ColumnLayout {
                    Layout.alignment: Qt.AlignRight
                    Layout.rightMargin: 10
                    Text {
                        text: "20:40"
                        font.pixelSize: 13
                        font.family: "Rubik"
                        //font.weight: Font.Bold
                    }
                    Image {
                        Layout.leftMargin: 5
                        source: "qrc:/images/header/checkmark.svg"
                        width: 18
                        height: 13
                    }
                }
            }
        }

        Rectangle {
            width: parent.width
            height: 75
            color: "#CCDCF3"
            RowLayout {
                Layout.fillWidth: true
                width: parent.width
                Image {
                    Layout.leftMargin: 10
                    Layout.topMargin: 7
                    source: "qrc:/images/main/chatProfile1.png"
                    width: 60
                    height: 60
                }
                ColumnLayout {
                    Layout.leftMargin: 10
                    Text {
                        text: "IL"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        //font.weight: Font.Bold
                    }
                    Text {
                       text: "Объяснишь домашку по питону?"
                       font.pixelSize: 13
                       font.family: "Rubik"
                    }
                }
                Item {
                    Layout.fillWidth: true
                }

                ColumnLayout {
                    Layout.alignment: Qt.AlignRight
                    Layout.rightMargin: 10
                    Text {
                        text: "12:38"
                        font.pixelSize: 13
                        font.family: "Rubik"
                        //font.weight: Font.Bold
                    }
                    Rectangle {
                        width: 30
                        height: 30
                        radius: 20
                        color: "#606FF1"
                        Text {
                            text: "1"
                            color: "white"
                            font.pixelSize: 16
                            font.family: "Rubik"
                            anchors.centerIn: parent
                        }
                    }
                }
            }
        }

        Rectangle {
            width: parent.width
            height: 75
            color: "#CCDCF3"
            RowLayout {
                Layout.fillWidth: true
                width: parent.width
                Image {
                    Layout.leftMargin: 10
                    Layout.topMargin: 7
                    source: "qrc:/images/main/chatProfile2.png"
                    width: 60
                    height: 60
                }
                ColumnLayout {
                    Layout.leftMargin: 10
                    Text {
                        text: "ПИ 14322"
                        font.pixelSize: 20
                        font.family: "Rubik"
                        //font.weight: Font.Bold
                    }
                    Text {
                       text: "Иван Петрушин: Леди и джентльмены, завтра..."
                       font.pixelSize: 13
                       font.family: "Rubik"
                    }
                }
                Item {
                    Layout.fillWidth: true
                }

                ColumnLayout {
                    Layout.alignment: Qt.AlignRight
                    Layout.rightMargin: 10
                    Text {
                        text: "20:40"
                        font.pixelSize: 13
                        font.family: "Rubik"
                        //font.weight: Font.Bold
                    }
                    Rectangle {
                        width: 30
                        height: 30
                        radius: 20
                        color: "#606FF1"
                        Text {
                            text: "130"
                            anchors.centerIn: parent
                            color: "white"
                            font.pixelSize: 10
                            font.family: "Rubik"
                        }
                    }
                }
            }
        }
        Item {
            Layout.fillHeight: true
        }

        Rectangle {
            width: 20
            height: 20
            color: "transparent"
            Layout.alignment:  Qt.AlignBottom | Qt.AlignRight
            Layout.rightMargin: 100
            Layout.bottomMargin: -5

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/closeAdIcon.svg"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Banner closed!")
                }
            }
        }

        Rectangle {
            width: 430
            Layout.preferredHeight: 100
            Layout.alignment: Qt.AlignBottom
            Layout.bottomMargin: 18

            Image {
                anchors.fill: parent
                source: "qrc:/images/main/adBanner.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Banner clicked!")
                    }
                }
            }
        }
    }

    Drawer {
        id: chat
        width: 430
        height: parent.height

        Rectangle {
            width: parent.width
            height: parent.height
            color: "#CCDCF3"
        }

        ColumnLayout {
            spacing: 0
            anchors.fill: parent
            Rectangle {
                Layout.alignment: Qt.AlignTop
                width: 430
                height: 80
                color: "white"
                RowLayout {
                    Layout.fillWidth: true
                    width: parent.width
                    height: parent.height
                    Image {
                        Layout.leftMargin: 10
                        source: "qrc:/images/header/backArrowNegativeIcon.svg"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: chat.close()
                        }
                    }
                    Image {
                        Layout.leftMargin: 10
                        Layout.topMargin: 7
                        source: "qrc:/images/main/chatProfile0.png"
                        width: 60
                        height: 60
                    }
                    ColumnLayout {
                        Layout.leftMargin: 10
                        Text {
                            text: "Vloodek"
                            font.pixelSize: 20
                            font.family: "Rubik"
                            //font.weight: Font.Bold
                        }
                        Text {
                           text: "был в сети в 20:45"
                           font.pixelSize: 13
                           font.family: "Rubik"
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    Image {
                        source: "qrc:/images/main/breadcrumbs.svg"
                        Layout.rightMargin: 10
                        Layout.alignment: Qt.AlignRight
                    }
                }
            }
            Rectangle {
                width: 430
                height: 2
                color: "#606FF1"
            }
            Item {
                Layout.fillHeight: true
            }

            Rectangle {
                Layout.alignment: Qt.AlignHCenter
                Layout.bottomMargin: 15
                width: 80
                height: 20
                radius: 20
                color: "#606FF1"
                Text {
                    text: "Сегодня"
                    anchors.centerIn: parent
                    color: "white"
                    font.pixelSize: 13
                }
            }

            Rectangle {
                Layout.leftMargin: 15
                width: 300
                height: 50
                radius: 20
                color: "#606FF1"
                Layout.bottomMargin: 15
                RowLayout {
                    Layout.fillWidth: true
                    width: parent.width
                    height: parent.height
                    Text {
                        Layout.leftMargin: 10
                        text: "Приветствую вас, чемпион"
                        color: "white"
                        font.pixelSize: 16
                    }
                    Text {
                        Layout.rightMargin: 10
                        text: "20:40"
                        color: "#C0C0C0"
                        font.pixelSize: 10
                    }
                }
                Rectangle {
                    width: 20
                    height: 20
                    color: "#606FF1"
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                }
            }

            Rectangle {
                width: parent.width
                height: 45
                color: "#606FF1"
                RowLayout {
                    anchors.fill: parent
                    width: parent.width
                    height: parent.height
                    Image {
                        source: "qrc:/images/main/emojie.svg"
                        Layout.leftMargin: 10
                    }

                    TextField {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        verticalAlignment: TextInput.AlignVCenter
                        background: Rectangle {
                            color: "#606FF1"
                        }
                        height: parent.height
                        width: parent.width
                        placeholderText: "Сообщение"
                        font.pixelSize: 16
                        font.family: "Rubik"
                        color: "white"
                    }

                    Image {
                        source: "qrc:/images/main/file.svg"
                        Layout.rightMargin: 10
                        Layout.alignment: Qt.AlignRight
                    }
                }
            }
        }
    }

    footer: PageFooter {}
}
