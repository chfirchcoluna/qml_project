import QtQuick 2.15
import QtQuick.Controls 2.15


ApplicationWindow {
    visible: true
    width: 430
    height: 932
    title: "Study Hub"
    color: "white"

    Loader {
        id: windowLoader
        anchors.fill: parent
        source: "EnterPage.qml"
    }
}
