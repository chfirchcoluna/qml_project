import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Controls.Material 2.15


Page {
    Rectangle {
        width: parent.width
        height: parent.height
        color: "white"
    }

    Rectangle {
        id: loaderRect
        width: parent.width * 1
        height: parent.height * 0.65

        SequentialAnimation {
            id: loadingAnimation
            running: true

            ParallelAnimation {
                PropertyAnimation { target: square0; property: "rotation"; to: 90; duration: 600 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square1; property: "rotation"; to: -90; duration: 550 }
                PropertyAnimation { target: square2; property: "rotation"; to: -90; duration: 550 }
                PropertyAnimation { target: square3; property: "rotation"; to: -90; duration: 550 }
                PropertyAnimation { target: square4; property: "rotation"; to: -90; duration: 550 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square0; property: "rotation"; to: 180; duration: 500 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square1; property: "rotation"; to: -180; duration: 450 }
                PropertyAnimation { target: square2; property: "rotation"; to: -180; duration: 450 }
                PropertyAnimation { target: square3; property: "rotation"; to: -180; duration: 450 }
                PropertyAnimation { target: square4; property: "rotation"; to: -180; duration: 450 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square0; property: "rotation"; to: 270; duration: 400 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square1; property: "rotation"; to: -270; duration: 350 }
                PropertyAnimation { target: square2; property: "rotation"; to: -270; duration: 350 }
                PropertyAnimation { target: square3; property: "rotation"; to: -270; duration: 350 }
                PropertyAnimation { target: square4; property: "rotation"; to: -270; duration: 350 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square0; property: "rotation"; to: 360; duration: 300 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square1; property: "rotation"; to: -360; duration: 250 }
                PropertyAnimation { target: square2; property: "rotation"; to: -360; duration: 250 }
                PropertyAnimation { target: square3; property: "rotation"; to: -360; duration: 250 }
                PropertyAnimation { target: square4; property: "rotation"; to: -360; duration: 250 }
            }

            PauseAnimation { duration: 200 }

            ParallelAnimation {
                PropertyAnimation { target: square1; properties: "opacity"; to: 0; duration: 200 }
                PropertyAnimation { target: square2; properties: "opacity"; to: 0; duration: 200 }
                PropertyAnimation { target: square3; properties: "opacity"; to: 0; duration: 200 }
                PropertyAnimation { target: square4; properties: "opacity"; to: 0; duration: 200 }
                PropertyAnimation { target: registrationButton; property: "opacity"; to: 1; duration: 200 }
                PropertyAnimation { target: loginButton; property: "opacity"; to: 1; duration: 200 }
                PropertyAnimation { target: withoutAccountButton; property: "opacity"; to: 1; duration: 200 }
            }
        }

            Rectangle {
                id: square0
                x: 80
                y: 166
                width: 270
                height: 270

                Rectangle {
                    id: square1
                    z: 3
                    x: 0
                    y: 0
                    width: 135
                    height: 135
                    Image {
                            id: svg1Image
                            source: "qrc:/images/anim1box.svg"
                            width: 135
                            height: 135
                            fillMode: Image.PreserveAspectFit
                        }

                }

                Rectangle {
                    id: square2
                    x: 135
                    y: 0
                    width: 135
                    height: 135
                    Image {
                            id: svg2Image
                            source: "qrc:/images/anim2box.svg"
                            width: 135
                            height: 135
                            fillMode: Image.PreserveAspectFit
                        }
                }

                Rectangle {
                    id: square3
                    z: 3
                    x: 135
                    y: 135
                    width: 135
                    height: 135
                    Image {
                            id: svg3Image
                            source: "qrc:/images/anim3box.svg"
                            width: 135
                            height: 135
                            fillMode: Image.PreserveAspectFit
                        }
                }

                Rectangle {
                    id: square4
                    x: 0
                    y: 135
                    width: 135
                    height: 135
                    Image {
                            id: svg4Image
                            source: "qrc:/images/anim4box.svg"
                            width: 135
                            height: 135
                            fillMode: Image.PreserveAspectFit
                        }
                }
            }
        ColumnLayout {
            width: parent.width * 0.62
            id: buttonsLayout
            anchors.centerIn: parent
            spacing: -30

            Layout.margins: 0

            Button {
                id: registrationButton
                text: "Регистрация"
                font.pixelSize: 24
                background: Rectangle {
                    id: registrationButtonBackground
                    color: "white"
                    radius: 20
                    Rectangle {
                        id: clipped
                        width: parent.width
                        height: parent.height + radius
                        radius: 20
                        border.color: "#cccccc"
                        border.width: 2
                    }
                }
                contentItem: Text {
                    text: registrationButton.text
                    font.pixelSize: registrationButton.font.pixelSize
                    font.bold: registrationButton.font.bold
                    font.family: registrationButton.font.family
                    bottomPadding: 16
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                font.bold: true
                font.family: "Roboto Mono"
                opacity: 0
                Layout.preferredHeight: 100
                Layout.fillWidth: true
            }

            Button {
                id: loginButton
                text: "Вход"
                z: 3
                background: Rectangle {
                    color: "white"
                    border.color: "#cccccc"
                    border.width: 2
                }
                font.pixelSize: 24
                font.bold: true
                font.family: "Roboto Mono"
                opacity: 0
                Layout.preferredHeight: 80
                Layout.fillWidth: true
            }

            Button {
                id: withoutAccountButton
                onClicked: windowLoader.source = "MainPage.qml"
                // onClicked: loadPage("Registration Page", "MainPage.qml")
                text: "Войти без\nаккаунта"
                background: Rectangle {
                    id: withoutAccountButtonBackground
                    color: "white"
                    radius: 20
                    Rectangle {
                        id: clipped2
                        width: parent.width
                        height: parent.height + radius
                        radius: withoutAccountButtonBackground.radius
                        border.color: "#cccccc"
                        border.width: 2
                    }
                }

                font.pixelSize: 24
                font.bold: true
                contentItem: Text {
                    text: withoutAccountButton.text
                    font.pixelSize: withoutAccountButton.font.pixelSize
                    font.bold: withoutAccountButton.font.bold
                    font.family: withoutAccountButton.font.family
                    topPadding: 30
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                font.family: "Roboto Mono"
                opacity: 0
                Layout.preferredHeight: 118
                Layout.fillWidth: true
            }
        }



    }

    ColumnLayout {
        anchors.bottom: parent.bottom
        width: parent.width * 1
        Text {
            text: "Study"
            font.family: "Karla"
            Layout.alignment: Qt.AlignHCenter
            font.bold: true
            color: "#606FF1"
            font.pixelSize: 96
        }
        Text {
            text: "Hub"
            font.family: "Karla"
            Layout.alignment: Qt.AlignHCenter
            font.bold: true
            color: "#606FF1"
            font.pixelSize: 96
            bottomPadding: 210
        }
    }

    Component.onCompleted: {
        loadingAnimation.start()
    }

}

