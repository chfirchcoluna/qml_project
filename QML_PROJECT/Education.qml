import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Page {
    width: 430

    header: PageHeader {}

    Rectangle {
        width: parent.width
        height: parent.height
        color: "white"
    }

    ColumnLayout {
        width: 430
        height: parent.height
        Item {
            Layout.fillHeight: true
        }

        RowLayout {
            Layout.bottomMargin: 20
            width: parent.width
            Layout.fillWidth: true
            height: 100
            Text {
                Layout.leftMargin: 15
                text: "Видеоролики"
                color: "#606FF1"
                font.pixelSize: 24
                font.bold: true
                font.family: "Rubik"
            }
            Item {
                Layout.fillWidth: true
            }

            Image {
                source: "qrc:/images/main/search.svg"
                Layout.rightMargin: 15
            }
            Image {
                source: "qrc:/images/main/filters.svg"
                Layout.rightMargin: 50
            }
        }

        RowLayout {
            width: parent.width
            height: 155
            Layout.leftMargin: 15
            Rectangle {
                width: 220
                height: parent.height
                border.color: "#606FF1"
                radius: 20
                border.width: 4
                ColumnLayout {
                    anchors.fill: parent
                    Image {
                        Layout.leftMargin: 4
                        Layout.topMargin: 4
                        width: 200
                        source: "qrc:/images/main/course0.png"
                    }
                    Rectangle {
                        width: parent.width
                        height: 2
                        color: "#606FF1"
                    }
                    Text {
                        Layout.alignment: Qt.AlignHCenter
                        Layout.bottomMargin: 10
                        text: "Питон для начинающих..."
                        font.pixelSize: 16
                        font.family: "Rubik"
                    }
                }
            }

            Rectangle {
                width: 220
                height: parent.height
                border.color: "#606FF1"
                radius: 20
                border.width: 4
                ColumnLayout {
                    anchors.fill: parent
                    Image {
                        Layout.leftMargin: 4
                        Layout.topMargin: 4
                        width: 200
                        source: "qrc:/images/main/course1.png"
                    }
                    Rectangle {
                        width: parent.width
                        height: 2
                        color: "#606FF1"
                    }
                    Text {
                        Layout.alignment: Qt.AlignHCenter
                        Layout.bottomMargin: 10
                        text: "Геометрия 10 класс в..."
                        font.pixelSize: 16
                        font.family: "Rubik"
                    }
                }
            }
        }

        Item {
            Layout.fillHeight: true
        }

        Text {
            Layout.leftMargin: 15
            text: "Наши курсы"
            color: "#606FF1"
            font.pixelSize: 24
            font.bold: true
            font.family: "Rubik"
        }

        RowLayout {
            width: parent.width
            height: 155
            Layout.leftMargin: 15
            Rectangle {
                color: "#CCDCF3"
                width: 220
                height: parent.height
                border.color: "#606FF1"
                radius: 20
                border.width: 4
                ColumnLayout {
                    width: parent.width
                    anchors.fill: parent
                    RowLayout {
                        width: parent.width
                        Rectangle {
                            Layout.leftMargin: 10
                            Layout.bottomMargin: 20
                            width: 125
                            Text {
                                text: "Программирование на Python для начинающих"
                                wrapMode: Text.WordWrap
                                width: parent.width
                            }
                        }
                        Item {
                            Layout.fillWidth: true
                        }

                        Image {
                            Layout.topMargin: 15
                            Layout.rightMargin: 10
                            source: "qrc:/images/main/courseImage0.png"
                        }
                    }
                    Item {
                        Layout.fillHeight: true
                    }

                    RowLayout {
                        width: parent.width
                        Layout.bottomMargin: 15
                        Text {
                            Layout.leftMargin: 10
                            text: "Бесплатно"
                            color: "#606FF1"
                            font.pixelSize: 12
                            font.bold: true
                            font.family: "Rubik"
                        }
                        Item {
                            Layout.fillWidth: true
                        }
                        Image {
                            source: "qrc:/images/main/time.svg"
                        }
                        Text {
                            text: "15ч"
                            font.pixelSize: 12
                            font.family: "Rubik"
                        }
                        Image {
                            source: "qrc:/images/main/star.svg"
                        }
                        Text {
                            Layout.rightMargin: 10
                            text: "5"
                            font.pixelSize: 12
                            font.family: "Rubik"
                        }
                    }
                }
            }

            Rectangle {
                color: "#CCDCF3"
                width: 220
                height: parent.height
                border.color: "#606FF1"
                radius: 20
                border.width: 4
                ColumnLayout {
                    width: parent.width
                    anchors.fill: parent
                    RowLayout {
                        width: parent.width
                        Rectangle {
                            Layout.leftMargin: 10
                            Layout.bottomMargin: 20
                            width: 125
                            Text {
                                text: "Программирование на Python для начинающих"
                                wrapMode: Text.WordWrap
                                width: parent.width
                            }
                        }
                        Item {
                            Layout.fillWidth: true
                        }

                        Image {
                            Layout.topMargin: 15
                            Layout.rightMargin: 10
                            source: "qrc:/images/main/courseImage0.png"
                        }
                    }
                    Item {
                        Layout.fillHeight: true
                    }

                    RowLayout {
                        width: parent.width
                        Layout.bottomMargin: 15
                        Text {
                            Layout.leftMargin: 10
                            text: "Бесплатно"
                            color: "#606FF1"
                            font.pixelSize: 12
                            font.bold: true
                            font.family: "Rubik"
                        }
                        Item {
                            Layout.fillWidth: true
                        }
                        Image {
                            source: "qrc:/images/main/time.svg"
                        }
                        Text {
                            text: "15ч"
                            font.pixelSize: 12
                            font.family: "Rubik"
                        }
                        Image {
                            source: "qrc:/images/main/star.svg"
                        }
                        Text {
                            Layout.rightMargin: 10
                            text: "5"
                            font.pixelSize: 12
                            font.family: "Rubik"
                        }
                    }
                }
            }
        }

        Item {
            Layout.fillHeight: true
        }

        Text {
            Layout.leftMargin: 15
            text: "Курсы партнёров"
            color: "#606FF1"
            font.pixelSize: 24
            font.bold: true
            font.family: "Rubik"
        }

        ListModel {
            id: myCourseModel
            ListElement {}
            ListElement {}
            ListElement {}
        }

        RowLayout {
            width: parent.width
            height: 155
            Layout.leftMargin: 15
            ListView {
                spacing: 5
                width: 430
                height: parent.height
                orientation: Qt.Horizontal
                model: myCourseModel
                delegate: CourseCardDelegate {}
            }
        }

        Item {
            Layout.fillHeight: true
        }
    }

    footer: PageFooter {}
}
