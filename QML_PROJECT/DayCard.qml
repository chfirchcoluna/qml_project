import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Page {
    id: myPage

    property alias comText: myHeader.text
    property var comDate: subDay(new Date())

    Rectangle {
        width: parent.width
        height: parent.height
        Rectangle {
            id: myRect
            x: 10
            width: parent.width - 20
            height: parent.height
            color: "#CCDCF3"
            radius: 10

            ColumnLayout {
                width: parent.width
                height: parent.height
                spacing: 10

                Rectangle {
                    width: parent.width
                    height: 37
                    radius: 10
                    color: "#606FF1"

                    Text {
                        id: myHeader
                        text: comDateToString(comDate)
                        anchors.centerIn: parent
                        font.pointSize: 20
                        font.family: "Rubik"
                        font.weight: Font.Bold
                        color: "white"
                    }
                }

                ListView {
                    width: parent.width
                    height: parent.height - 100
                    spacing: 15
                    model: myModel
                    delegate: RecordTaskDelegate {
                        delegColorBack: model.delegColorBack
                        delegColorText: model.delegColorText
                        delegSubject: model.delegSubject
                        delegTime: model.delegTime
                    }
                }

                ListModel {
                    id: myModel;
                    ListElement { delegColorBack: "#FF8484"; delegColorText: "white"; delegSubject: "Математика"; delegTime: "10:10 - 11:40"; }
                    ListElement { delegColorBack: "#84FFC4"; delegColorText: "white"; delegSubject: "Позвонить маме"; delegTime: "17:00 - 17:30"; }
                    ListElement { delegColorBack: "white"; delegColorText: "#F1C060"; delegSubject: "Экономика"; delegTime: "19:40 - 21:10"; }
                }

                Item {
                    Layout.fillHeight: true
                }


                Rectangle {
                    //Layout.fillWidth: true
                    Layout.preferredHeight: 44
                    Layout.preferredWidth: 44
                    Layout.alignment: Qt.AlignRight | Qt.AlignBottom
                    Layout.rightMargin: 6
                    Layout.bottomMargin: 9
                    Layout.topMargin: -15
                    color: "white"
                    radius: 100

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("Plus button clicked!")
                            popup.open()
                        }
                    }

                    Text {
                        anchors.centerIn: parent
                        text: "+"
                        color: "#606FF1"
                        font.pointSize: 19
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }
            }
        }
    }

    Popup {
        id: popup
        width: 430 - 60
        height: 932 - 60
        anchors.centerIn: parent
        opacity: 0
        modal: true
        Overlay.modal: Rectangle {
            color: "#1E1E1E00"
        }
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onOpened: {
            popup.opacity = 1;
        }

        onClosed: {
            popup.opacity = 0;
        }

        contentItem: Rectangle {
            width: parent.width
            height: parent.height
            anchors.centerIn: parent
            anchors.fill: parent

            ColumnLayout {
                width: parent.width
                height: parent.height
                spacing: 0

                Rectangle {
                    Layout.alignment: Qt.AlignTop
                    width: parent.width
                    height: 50
                    color: "#606FF1"

                    RowLayout {
                        anchors.fill: parent
                        spacing: 0

                        Text {
                            Layout.alignment: Qt.AlignHCenter
                            Layout.leftMargin: 20
                            text: "Добавить задачу"
                            font.pointSize: 24
                            font.family: "Rubik"
                            font.weight: Font.Bold
                            color: "white"
                        }

                        Image {
                            source: "qrc:/images/main/closeIcon.svg"
                            width: 15
                            height: 15
                            Layout.rightMargin: 5
                            MouseArea {
                                anchors.fill: parent
                                onClicked: popup.close()
                            }
                        }
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    text: "Название"
                    Layout.leftMargin: 38
                    font.pointSize: 20
                    font.family: "Rubik"
                    font.weight: Font.Bold
                }

                TextField {
                    Layout.leftMargin: 38
                    Layout.rightMargin: 38
                    Layout.fillWidth: true
                    Layout.preferredHeight: 44
                    background: Rectangle {
                        border.width: 0
                        radius: 10
                        width: parent.width
                        height: parent.height
                        color: "#CCDCF3"
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    text: "Предмет"
                    Layout.leftMargin: 38
                    font.pointSize: 20
                    font.family: "Rubik"
                    font.weight: Font.Bold
                }

                TextField {
                    Layout.leftMargin: 38
                    Layout.rightMargin: 38
                    Layout.fillWidth: true
                    Layout.preferredHeight: 44
                    background: Rectangle {
                        border.width: 0
                        radius: 10
                        width: parent.width
                        height: parent.height
                        color: "#CCDCF3"
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    text: "Описание"
                    Layout.leftMargin: 38
                    font.pointSize: 20
                    font.family: "Rubik"
                    font.weight: Font.Bold
                }

                TextArea {
                    Layout.leftMargin: 38
                    Layout.rightMargin: 38
                    Layout.fillWidth: true
                    Layout.preferredHeight: 128
                    background: Rectangle {
                        border.width: 0
                        radius: 10
                        width: parent.width
                        height: parent.height
                        color: "#CCDCF3"
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    text: "Дедлайн"
                    Layout.leftMargin: 38
                    font.pointSize: 20
                    font.family: "Rubik"
                    font.weight: Font.Bold
                }

                TextField {
                    Layout.leftMargin: 38
                    Layout.rightMargin: 38
                    Layout.fillWidth: true
                    Layout.preferredHeight: 44
                    background: Rectangle {
                        border.width: 0
                        radius: 10
                        width: parent.width
                        height: parent.height
                        color: "#CCDCF3"
                    }
                    Image {
                        x: 10
                        y: 10
                        source: "qrc:/images/main/calendarIcon2.svg"
                        width: 24
                        height: 24
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    text: "Промежуток времени"
                    Layout.leftMargin: 38
                    font.pointSize: 20
                    font.family: "Rubik"
                    font.weight: Font.Bold
                }

                TextField {
                    Layout.leftMargin: 38
                    Layout.rightMargin: 38
                    Layout.fillWidth: true
                    placeholderText: "Старт - Конец"
                    background: Rectangle {
                        border.width: 0
                        radius: 10
                        width: parent.width
                        height: parent.height
                        color: "#CCDCF3"
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Text {
                    text: "Список файлов"
                    Layout.leftMargin: 38
                    font.pointSize: 20
                    font.family: "Rubik"
                    font.weight: Font.Bold
                }


                RowLayout {
                    Layout.leftMargin: 38
                    Layout.rightMargin: 38
                    Layout.fillWidth: true
                    spacing: 0

                    Text {
                        Layout.alignment: Qt.AlignLeft
                        text: "1. economic_test.txt"
                    }

                    Item {
                        Layout.fillWidth: true
                    }


                    Image {
                        Layout.alignment: Qt.AlignRight
                        source: "qrc:/images/main/deleteIcon.svg"
                        width: 16
                        height: 16
                        MouseArea {
                            onClicked: {}
                        }
                    }

                }

                RowLayout {
                    Layout.topMargin: 5
                    Layout.leftMargin: 38
                    Layout.rightMargin: 38
                    Layout.fillWidth: true
                    spacing: 0

                    Text {
                        Layout.alignment: Qt.AlignLeft
                        text: "2. lections.pdf"
                    }

                    Item {
                        Layout.fillWidth: true
                    }


                    Image {
                        Layout.alignment: Qt.AlignRight
                        source: "qrc:/images/main/deleteIcon.svg"
                        width: 16
                        height: 16
                        MouseArea {
                            onClicked: {}
                        }
                    }

                }

                Item {
                    Layout.fillHeight: true
                }

                Rectangle {
                    width: 127
                    height: 33
                    color: "#606FF1"
                    radius: 23
                    Layout.leftMargin: 38
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {}
                    }

                    Text {
                        anchors.centerIn: parent
                        text: "Загрузить"
                        color: "white"
                        font.pointSize: 16
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }

                Item {
                    Layout.fillHeight: true
                }

                Rectangle {
                    width: 264
                    height: 76
                    color: "#606FF1"
                    radius: 23
                    Layout.bottomMargin: 30
                    Layout.alignment: Qt.AlignHCenter

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {}
                    }

                    Text {
                        anchors.centerIn: parent
                        text: "Добавить"
                        color: "white"
                        font.pointSize: 24
                        font.family: "Rubik"
                        font.weight: Font.Bold
                    }
                }


            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: popup.close()
        }
    }

    function subDay(date) {
        date.setDate(date.getDate() - 1);
        return date;
    }

    function updateComDate(date) {
        comDate = date;
    }

    function comDateToString(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return day + "." + month + "." + year;
    }
}
