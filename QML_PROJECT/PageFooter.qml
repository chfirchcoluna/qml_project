import QtQuick 2.15
import QtQuick.Layouts 1.15

Item {
    width: 430
    height: 87

    Rectangle {
        anchors.fill: parent
        color: "#606ff1"

        RowLayout {
            spacing: 0
            anchors.fill: parent
            Layout.alignment: Qt.AlignHCenter

            ColumnLayout {
                Layout.alignment: Qt.AlignHCenter

                Image {
                    source: "qrc:/images/footer/tasksIcon.svg"
                    Layout.alignment: Qt.AlignHCenter
                    width: 32
                    height: 32
                    Layout.leftMargin: 0
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                             windowLoader.source = "Tasks.qml"
                            console.log("Footer icon 'Задачи' clicked!")
                        }
                    }
                }

                Text {
                    color: "white"
                    font.weight: Font.Bold
                    font.family: "Rubik"
                    font.pixelSize: 12
                    text: "Задачи"
                    Layout.alignment: Qt.AlignHCenter
                }
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignHCenter

                Image {
                    source: "qrc:/images/footer/achievementsIcon.svg"
                    Layout.alignment: Qt.AlignHCenter
                    width: 32
                    height: 32

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            windowLoader.source = "Achievements.qml"
                            console.log("Footer icon 'Достижения' clicked!")
                        }
                    }
                }

                Text {
                    color: "white"
                    font.weight: Font.Bold
                    font.family: "Rubik"
                    font.pixelSize: 12
                    text: "Достижения"
                    Layout.alignment: Qt.AlignHCenter
                }
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignHCenter

                Image {
                    source: "qrc:/images/footer/mainIcon.svg"
                    Layout.alignment: Qt.AlignHCenter
                    width: 32
                    height: 32

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            windowLoader.source = "MainPage.qml"
                            console.log("Footer icon 'Главная' clicked!")
                        }
                    }
                }

                Text {
                    color: "white"
                    font.weight: Font.Bold
                    font.family: "Rubik"
                    font.pixelSize: 12
                    text: "Главная"
                    Layout.alignment: Qt.AlignHCenter
                }
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignHCenter

                Image {
                    source: "qrc:/images/footer/forumIcon.svg"
                    Layout.alignment: Qt.AlignHCenter
                    width: 32
                    height: 32

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            windowLoader.source = "Chat.qml"
                            console.log("Footer icon 'Общение' clicked!")
                        }
                    }
                }

                Text {
                    color: "white"
                    font.weight: Font.Bold
                    font.family: "Rubik"
                    font.pixelSize: 12
                    text: "Общение"
                    Layout.alignment: Qt.AlignHCenter
                }
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignHCenter

                Image {
                    source: "qrc:/images/footer/educationIcon.svg"
                    Layout.alignment: Qt.AlignHCenter
                    width: 32
                    height: 32

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            windowLoader.source = "Education.qml"
                            console.log("Footer icon 'Обучение' clicked!")
                        }
                    }
                }

                Text {
                    color: "white"
                    font.weight: Font.Bold
                    font.family: "Rubik"
                    font.pixelSize: 12
                    text: "Обучение"
                    Layout.alignment: Qt.AlignHCenter
                }
            }
        }
    }
}
